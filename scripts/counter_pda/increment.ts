import * as anchor from "@coral-xyz/anchor"
import { getKeypairFromEnvironment } from "@solana-developers/helpers";
import idl from '../../counterpda.json';
import { Program, Wallet } from "@coral-xyz/anchor";
import "dotenv/config";
import * as web3 from "@solana/web3.js";

const PROGRAM_ID = 'EfoK5pZzVStnQ9hUM7KMwim6oGUGg35A6uwVqrg9ReY8';
const programPublicKey = new web3.PublicKey(PROGRAM_ID);
const SYSTEM_PROGRAM_ID = '11111111111111111111111111111111';
const systemProgramId = new web3.PublicKey(SYSTEM_PROGRAM_ID);

const keypair = await getKeypairFromEnvironment('SECRET_KEY');
const wallet = new Wallet(keypair);
const connection = new web3.Connection(web3.clusterApiUrl('devnet'));
const provider = new anchor.AnchorProvider(connection, wallet, {});

const [counterPubkey, _] =
    await anchor.web3.PublicKey.findProgramAddress(
        [wallet.publicKey.toBytes()],
        programPublicKey
    );

const program = new Program(idl as anchor.Idl, provider);
const counter = await program.account.counter.fetch(counterPubkey);

const tx = await program.methods
      .updateCounter()
      .accounts({
        counter: counterPubkey,
      })
      .rpc();

console.log(tx);
