import * as anchor from "@coral-xyz/anchor"
import { getKeypairFromEnvironment } from "@solana-developers/helpers";
import idl from '../../dao_vote_alyra.json';
import { Program, Wallet } from "@coral-xyz/anchor";
import "dotenv/config";
import * as web3 from "@solana/web3.js";
import { PublicKey } from "@solana/web3.js";

const SYSTEM_PROGRAM_ID = '11111111111111111111111111111111';
const systemProgramId = new web3.PublicKey(SYSTEM_PROGRAM_ID);

const keypair = await getKeypairFromEnvironment('SECRET_KEY');
const wallet = new Wallet(keypair);
const connection = new web3.Connection(web3.clusterApiUrl('devnet'));
const provider = new anchor.AnchorProvider(connection, wallet, {});
const program = new Program(idl as anchor.Idl, provider);
const programPublicKey = new web3.PublicKey(program.programId);

console.log(programPublicKey.toBase58());

const [proposalPubkey, _] =
  await anchor.web3.PublicKey.findProgramAddress(
    [wallet.publicKey.toBytes()],
    programPublicKey
  );

const proposal = await program.account.proposal.all();
// console.log('proposal -> ', proposal);

const bcAccountPubkey = new web3.PublicKey('8R6U1mEkNnmXZLuDjrkVknAF5MWCwmRTyMe2fs2x9K15');
const bcProposal = await program.account.proposal.fetch(bcAccountPubkey);
console.log(bcProposal);

/*
const getVoterAddress = async (votePublicKey, userPublicKey) => {
  return (
    await PublicKey.findProgramAddress(
      [votePublicKey.toBuffer(), userPublicKey.toBuffer()],
      programPublicKey
    )
  )[0];
};
*/
const voterAddress = await getVoterAddress(bcAccountPubkey, wallet.publicKey);

const tx = await program.methods
      .vote(1)
      .accounts({
        proposal: bcAccountPubkey,
        voter: voterAddress,
        signer: wallet.publicKey,
        systemProgram: systemProgramId,
      })
      .signers([keypair])
      .rpc();

console.log(tx);

async function getVoterAddress(votePublicKey: PublicKey, userPublicKey: PublicKey) {
  return PublicKey.findProgramAddressSync(
    [votePublicKey.toBuffer(), userPublicKey.toBuffer()],
    programPublicKey
  )[0];
}
