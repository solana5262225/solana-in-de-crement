import * as anchor from "@coral-xyz/anchor"
import { getKeypairFromEnvironment } from "@solana-developers/helpers";
import idl from '../../dao_vote_alyra.json';
import { Program, Wallet } from "@coral-xyz/anchor";
import "dotenv/config";
import * as web3 from "@solana/web3.js";
import BN from 'bn.js';
import { Keypair } from "@solana/web3.js";

const SYSTEM_PROGRAM_ID = '11111111111111111111111111111111';
const systemProgramId = new web3.PublicKey(SYSTEM_PROGRAM_ID);

const keypair = await getKeypairFromEnvironment('SECRET_KEY');
const wallet = new Wallet(keypair);
const connection = new web3.Connection(web3.clusterApiUrl('devnet'));
const provider = new anchor.AnchorProvider(connection, wallet, {});
const program = new Program(idl as anchor.Idl, provider);
const programPublicKey = new web3.PublicKey(program.programId);

const [proposal, _proposalBump] =
    await anchor.web3.PublicKey.findProgramAddress(
        [wallet.publicKey.toBytes()],
        programPublicKey
    );

const title = 'Un titre depuis le front';
const description = 'une description front';
const choices = ["Solana", "Bitcoin", "Cosmos", "Polkadot"];

const currentYear = new Date().getFullYear();
const endOfYearDate = new Date(currentYear, 11, 31); // Note that month is 0-indexed, so 11 represents December
endOfYearDate.setUTCHours(23, 59, 59, 999); // Set to the last millisecond of the day

const finalTimeStamp = Math.floor(endOfYearDate.getTime() / 1000); // Convert to seconds since Unix epoch
const deadline = new BN.BN(finalTimeStamp);

const proposalKeypair = Keypair.generate();

const tx = await program.methods
    .createProposal(title, description, choices, deadline)
    .accounts({
        proposal: proposalKeypair.publicKey,
        signer: wallet.publicKey,
        systemProgram: systemProgramId,
    })
    .signers([proposalKeypair])
    .rpc();

console.log(tx);
