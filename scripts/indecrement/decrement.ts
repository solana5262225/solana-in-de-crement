import * as anchor from "@coral-xyz/anchor"
import { getKeypairFromEnvironment } from "@solana-developers/helpers";
import idl from '../../idl.json';
import { Program, Wallet } from "@coral-xyz/anchor";
import "dotenv/config";
import * as web3 from "@solana/web3.js";

const keypair = await getKeypairFromEnvironment('SECRET_KEY');
const wallet = new Wallet(keypair);
const connection = new web3.Connection(web3.clusterApiUrl('devnet'));
const provider = new anchor.AnchorProvider(connection, wallet, {});
const counterPublickey = new web3.PublicKey("CAx5qEQ1zRz8drLVRpzntjHi1eqDdpbCgUkGjLwwBXRf");
const program = new Program(idl as anchor.Idl, provider);

const tx = await program.methods
.decrement()
.accounts({ counter: counterPublickey })
.signers([keypair])
.rpc();

console.log(tx);
