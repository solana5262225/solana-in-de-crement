import { NextPage } from 'next'
import styles from '../styles/Home.module.css'
import WalletContextProvider from '../components/WalletContextProvider'
import { AppBar } from '../components/AppBar'
import Head from 'next/head'

const appName = "Solana frontend starter";

const Home: NextPage = (props) => {

  return (
    <div className={styles.App}>
      <Head>
        <title>{appName}</title>
        <meta
          name="description"
          content={appName}
        />
      </Head>
      <WalletContextProvider>
        <AppBar />
      </WalletContextProvider >
    </div>
  );
}

export default Home;